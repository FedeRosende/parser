from sys import stdin
from parser import parser

s = stdin.read() # Ctrl+D signifies the end of the input when typed at the start of a line on a terminal

result = parser.parse(s)
print(result)