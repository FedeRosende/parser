'''

GRAMMAR 

----------------------------------------------
<S>                 : <PGNGROUP>
<PGNGROUP>          : <PGN><PGNGROUP>                   | <PGN>
<PGN>               : <TAGS><GAME>
<TAGS>              : <TAGPAIR><TAGS>                   | <TAGPAIR>
<TAGPAIR>           : OPSQBRACK <TEXT>STRING CLSQBRACK
<TEXT>              : WORD<TEXT>                        | WORD
<GAME>              : <MOVES><END>
<MOVES>             : MVNUM MOVE<BLACK>                 | MVNUM MOVE<FULLCOMMENT><BLACKAFTERCOMMENT>    | lambda            
<BLACK>             : MOVE<MOVES>                       | MOVE<FULLCOMMENT><MOVES>                      | lambda            
<BLACKAFTERCOMMENT> : MVNUM MOVE<MOVES>                 | MVNUM MOVE<FULLCOMMENT><MOVES>                | lambda            
<FULLCOMMENT>       : OPCOMM<COMMENT>CLCOMM
<COMMENT>           : <CONTENT><COMMENT>                | lambda
<CONTENT>           : WORD                              | MVNUM MOVE                                    | MOVE          | <FULLCOMMENT>
<END>               : RES
'''

import ply.yacc as yacc
from lexer import tokens
import operator

appearances = {}

#S
def p_s(p):
    '''s : pgngroup'''
    maxplay = "No hay jugadas en la partida."
    if(appearances):
        play = max(appearances, key=appearances.get)
        maxplay = "La jugada " + play + " fue la más usada para comenzar con " + str(appearances[play]) + (" apariciones." if appearances[play]>1 else " aparición.")

    p[0] = "PGN ACEPTADO" + "\n" + "Máxima profundidad de comentarios con jugadas " + str(p[1]) + ".\n" + maxplay

#PGNGROUP
def p_pgngroup(p):
    '''pgngroup : pgn pgngroup'''
    p[0] = max(p[1], p[2])

def p_pgngroup_single(p):
    '''pgngroup : pgn'''
    p[0] = p[1]

#PGN
def p_pgn(p):
    '''pgn : tags game'''
    global appearances
    p[0] = p[2][1]
    if(not p[2][2] == 'non-move'):
        if(not p[2][2] in appearances): appearances[p[2][2]] = 0
        appearances[p[2][2]] += 1

#TAGS
def p_tags(p):
    '''tags : tagpair tags'''       
    return

def p_tags_tagpair(p):
    '''tags : tagpair'''
    pass

#TAGPAIR
def p_tagpair(p):
    '''tagpair : OPSQBRACK text STRING CLSQBRACK'''
    pass 

#TEXT
def p_text(p):
    '''text : WORD text'''
    pass

def p_text_word(p):
    '''text : WORD'''
    pass

#GAME
def p_game(p):
    '''game : moves end'''
    p[0] = p[1]

#MOVES
def p_moves(p):
    '''moves : MVNUM MOVE black'''
    p[0] = ('move', p[3][1], p[2])


def p_moves_empty(p):
    '''moves : '''
    p[0] = ('empty', 0, 'non-move')

def p_moves_comment(p):
    '''moves : MVNUM MOVE fullcomment blackaftercomment'''
    p[0] = ('move', max(p[3][1], p[4][1]), p[2])


#BLACK
def p_black(p):
    '''black : MOVE moves'''
    p[0] = p[2]

def p_black_comment(p):
    '''black : MOVE fullcomment moves'''
    p[0] = ('move', max(p[2][1], p[3][1]))


def p_black_empty(p):
    '''black : '''
    p[0] = ('empty', 0)

#BLACKAFTERCOMMENTS
def p_blackaftercomment(p):
    '''blackaftercomment : MVNUM MOVE moves'''
    p[0] = p[3]


def p_blackaftercomment_comment(p):
    '''blackaftercomment : MVNUM MOVE fullcomment moves'''
    p[0] = ('move', max(p[3][1], p[4][1]))


def p_blackaftercomment_empty(p):
    '''blackaftercomment : '''
    p[0] = ('empty', 0)
 

#FULLCOMMENT
def p_fullcomment(p):
    '''fullcomment : OPCOMM comment CLCOMM'''
    if((p[1] == '{' and p[3] == ')') or (p[1] == '(' and p[3] == '}')): p_error(p[1])
    p[0] = p[2]

#COMMENT
def p_comment(p):
    '''comment : content comment'''
    if(p[1][0] == 'move' or p[2][0] == 'move'):
        p[0] = ('move', max(p[1][1], p[2][1]))
    else:
        p[0] = p[1]

def p_comment_empty(p):
    '''comment : '''
    p[0] = ('empty', 0)

#CONTENT
def p_content_word(p):
    '''content : WORD'''
    p[0] = ('word', 0)

def p_content_mvnum(p):
    '''content : MVNUM MOVE'''
    p[0] = ('move', 1)

def p_content_move(p):
    '''content : MOVE'''
    p[0] = ('move', 1)

def p_content_comment(p):
    '''content : fullcomment'''
    if(p[1][0] == 'move'): 
        p[0] = ('move', p[1][1] + 1)
    else:
        p[0] = p[1]

#END
def p_end(p):
    '''end : RES'''
    p[0] = (p[1], 0)

#ERROR
class ParserError(Exception): pass

def p_error(p):
    msg = "Error sintáctico: "
    if (p):
     if(isinstance(p, str)): msg += p
     else: msg += p.value
    else:
     msg += "fin de archivo"
    raise ParserError(msg)


parser = yacc.yacc()

if __name__ == "__main__":
    import logging
    logging.basicConfig(
        level = logging.DEBUG,
        filename = "parselog.txt",
        filemode = "w",
        format = "%(filename)10s:%(lineno)4d:%(message)s"
    )
    log = logging.getLogger()
     
    yacc.yacc(debug=True,debuglog=log)
    s = '''
    [Event "Mannheim"]
    [Site "Mannheim GER"]
    [Date "1914.08.01"]
    [EventDate "1914.07.20"]
    [Round "11"]
    [Result "1-0"]
    [White "Alexander Alekhine"]
    [Black "Hans Fahrni"]
    [ECO "C13"]
    [WhiteElo "?"]
    [BlackElo "?"]
    [PlyCount "45"]
    1-0
    '''
    result = parser.parse(s, debug=log)
    print(result)
