for file in ./tests/*.pgn
do
    echo "Input:"
    cat $file
    echo "Output:"
    python3 main.py < $file
    echo
done
